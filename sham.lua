local Tinkr = ...
local Routine = Tinkr.Routine

-- Once automatically loaded, enable in-game with
-- /routine load sham




Routine:RegisterRoutine(function()
    if gcd() > latency() then
        return
    end

    if not latencyCheck() then
        return
    end

    if mounted() then
        return
    end

    if UnitExists('target') and not UnitIsDeadOrGhost('target') and enemy('target') then
        if not IsPlayerAttacking('target') then
            StartAttack()
        end

	if combat() and not buff(LightningShield) then
        return cast(LightningShield)
	end       
       
	-- Interupt
	
	if casting('target') then 
  		return cast(EarthShock, 'target')

	end 
       
        -- stormstrike
        
        if combat() and castable(Stormstrike) then
            return cast(Stormstrike)
        end

end

end, 2, 'sham')
